json.extract! raid, :id, :event_date, :location, :dead_civilian, :race, :dead_officer_1, :dead_officer_2, :standard_warrant, :no_knock_warrant, :warrant_type_unknown, :video, :source, :created_at, :updated_at
json.url raid_url(raid, format: :json)
