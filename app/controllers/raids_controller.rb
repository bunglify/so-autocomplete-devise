class RaidsController < ApplicationController
  autocomplete :raid, :location, :scopes => [:uniquely_placed]
  skip_before_action :authenticate_user!, only: [:index, :autocomplete_raid_location]

  before_action :set_raid, only: [:show, :edit, :update, :destroy]

  def index
    @raids = Raid.all
    if params[:search]
      @raids = Raid.where('location like ?', "%#{params[:search]}%")
    end
  end

  def show
  end

  def new
    @raid = Raid.new
  end

  def edit
  end

  def create
    @raid = Raid.new(raid_params)

    respond_to do |format|
      if @raid.save
        format.html { redirect_to @raid, notice: 'Raid was successfully created.' }
        format.json { render :show, status: :created, location: @raid }
      else
        format.html { render :new }
        format.json { render json: @raid.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @raid.update(raid_params)
        format.html { redirect_to @raid, notice: 'Raid was successfully updated.' }
        format.json { render :show, status: :ok, location: @raid }
      else
        format.html { render :edit }
        format.json { render json: @raid.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @raid.destroy
    respond_to do |format|
      format.html { redirect_to raids_url, notice: 'Raid was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_raid
    @raid = Raid.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def raid_params
    params.require(:raid).permit(:event_date, :location, :dead_civilian, :race, :dead_officer_1, :dead_officer_2, :standard_warrant, :no_knock_warrant, :warrant_type_unknown, :video, :source)
  end
end
