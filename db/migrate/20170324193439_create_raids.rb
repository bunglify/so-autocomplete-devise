class CreateRaids < ActiveRecord::Migration
  def change
    create_table :raids do |t|
      t.date :event_date
      t.string :location
      t.string :dead_civilian
      t.string :race
      t.string :dead_officer_1
      t.string :dead_officer_2
      t.boolean :standard_warrant, default: false
      t.boolean :no_knock_warrant, default: false
      t.boolean :warrant_type_unknown, default: false
      t.string :video
      t.string :source

      t.timestamps null: false
    end
  end
end
