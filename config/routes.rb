Rails.application.routes.draw do

  devise_for :users
  resources :users
  resources :raids do
    get :autocomplete_raid_location, :on => :collection

  end
  root 'welcome#index'

end
