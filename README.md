# README #

This is a barebones application to illustrate how to allow rails4-autocomplete calls to work with devise

### Why is this a problem? ###

If you are creating an application that has very few public pages it is not uncommon to to place the below snippet in your `application_controller`

    before_action :authenticate_user!, unless: :devise_controller?

However, if you want to allow a search feature of any kind that uses autocomplete that does not require a login, you'll need to make sure that action is whitelisted in your controller.

    skip_before_action :authenticate_user!, only: [:index, :autocomplete_<model>_<field>]


### How do I set up? ###

**Basic steps**

* Download the app
* bundle install
* bundle exec rake db:migrate

**Database population**

You can add data to test with from the seeds file

* bundle exec rake db:seed

**Start the server**

* rails s 

Will run a unicorn server on port 3000

### Attribution ###

* The data for this db was sourced from: https://github.com/newsdev/nyt-forcible-entry/
* The Stack Overflow question was: http://stackoverflow.com/questions/43007120/rails-4-autocomplete-and-devise-authorization