require 'test_helper'

class RaidsControllerTest < ActionController::TestCase
  setup do
    @raid = raids(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:raids)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create raid" do
    assert_difference('Raid.count') do
      post :create, raid: { dead_civilian: @raid.dead_civilian, dead_officer_1: @raid.dead_officer_1, dead_officer_2: @raid.dead_officer_2, event_date: @raid.event_date, location: @raid.location, no_knock_warrant: @raid.no_knock_warrant, race: @raid.race, source: @raid.source, standard_warrant: @raid.standard_warrant, video: @raid.video, warrant_type_unknown: @raid.warrant_type_unknown }
    end

    assert_redirected_to raid_path(assigns(:raid))
  end

  test "should show raid" do
    get :show, id: @raid
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @raid
    assert_response :success
  end

  test "should update raid" do
    patch :update, id: @raid, raid: { dead_civilian: @raid.dead_civilian, dead_officer_1: @raid.dead_officer_1, dead_officer_2: @raid.dead_officer_2, event_date: @raid.event_date, location: @raid.location, no_knock_warrant: @raid.no_knock_warrant, race: @raid.race, source: @raid.source, standard_warrant: @raid.standard_warrant, video: @raid.video, warrant_type_unknown: @raid.warrant_type_unknown }
    assert_redirected_to raid_path(assigns(:raid))
  end

  test "should destroy raid" do
    assert_difference('Raid.count', -1) do
      delete :destroy, id: @raid
    end

    assert_redirected_to raids_path
  end
end
